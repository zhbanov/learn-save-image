import React from "react";

/**
 * Прорисовка каждого изображения из массива.
 */
export class EveryImage extends React.Component {

  /**
   * Обработчик отвечающий за изменение значения.
   * 
   * @param {string} element Значение содержимого input.
   */
  handleClick = (url) => () => {
    this.props.onHandleClick(url);
  }

  render() {

    const url = this.props.data.url;

    return (
      <img className="find_images" src={url} onClick={this.handleClick(url)}></img>
    );
  }
};