import React from "react";
import { Images } from "./Images";

/**
 * Генерация GET запроса на сервер и обработка ответа.
 */
export class TestInput extends React.Component {
    constructor() {
        super();
        this.state = {
            onChange: false,
            myValue: '',
            jsonFile: ''
        }

    }

    /**
     * Генерация адреса сервера исходя из введенного в поле input значения. 
     */
    getURI = () => {
        const postfix = (!!this.state.myValue ? this.state.myValue : 'notebook');

        return 'http://www.splashbase.co/api/v1/images/search?query=' + postfix;
    }

    /**
     * Отправка GET запроса на сервер и запись JSON ответа в состояние jsonFile.
     */
    RequestTwitter = () => {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', this.getURI(), false);
        xhr.send();
        if (xhr.status === 200) {
            this.setState({
                jsonFile: JSON.parse(xhr.responseText)
            })
        }
    }

    /**
     * Обработчик отвечающий за изменение значения.
     * 
     * @param {string} element Значение содержимого input.
     */
    onChangeHandler = (element) => {
        this.setState({
            myValue: element.target.value
        });
        this.RequestTwitter()
    }

    /**
     * Обработчик, присваивающий значение url кликнутой картинки.
     * 
     * @param {string} url Параметр, содержащий url картинки.
     */
    handleClick = (url) => {
        this.props.onHandleClick(url);
    }

    render() {

        return (
            <div>
                <div>
                    <input
                        className='test-input'
                        // defaultValue=''
                        value={this.state.myValue}
                        onChange={this.onChangeHandler}
                        placeholder='notebook'
                    />
                </div>
                <Images
                    data={this.state.jsonFile}
                    onHandleClick={this.handleClick}
                />
            </div>
        );
    }
};
