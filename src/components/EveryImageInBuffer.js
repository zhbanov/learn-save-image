import React from "react";

/**
 * Отрисовка каждого изображения из буфера.
 */
export class EveryImageInBuffer extends React.Component {

    render() {
        const {data} = this.props;

        return (
            <img className="buffer_images" alt="saved_image" src={data}></img>
        );
    }
}