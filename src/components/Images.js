import React from "react";
import { EveryImage } from "./EveryImage";

/**
 * Обработка массива изображений из ответа сервера.
 */
export class Images extends React.Component {

  /**
   * Обработчик отвечающий за изменение значения.
   * 
   * @param {string} element Значение содержимого input.
   */
  handleClick = (url) => {
    this.props.onHandleClick(url);
  }

  render() {

    const data = this.props.data;
    let template;

    if (data.images !== undefined && data.images !== null && data.images.length > 0) {
        template =
          data.images.map((item, index) => {
            return (
              <a key={index}>
                <EveryImage
                  data={item}
                  onHandleClick={this.handleClick}
                />
              </a>
            );
          })
    }

    return (
      <div>
          {template}
      </div>
    );
  }
};