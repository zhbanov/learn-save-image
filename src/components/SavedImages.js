import React from "react";
import { EveryImageInBuffer } from "./EveryImageInBuffer";

/**
 * Отрисовка сохраненных изображений.
 */
export class SavedImages extends React.Component {

    render() {
        const { data } = this.props;

        if (!this.props.data) {
            return (
                <h3>Сюда можно сохранить понравившиеся картинки</h3>
            );
        }

        const template = data.map((item, index) => (
                <a key={index}>
                    <EveryImageInBuffer
                        data={item}
                    />
                </a>
                )
            );

        return (
            <div className="buffer" >
                {template}
            </div>
        );
    };
}