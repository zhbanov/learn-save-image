import "./dropdown.css";
import React from "react";
import ReactDOM from "react-dom";
import { TestInput } from "./components/TestInput";
import { SavedImages } from "./components/SavedImages";

/**
 * Отрисовывает всю страницу.
 */
class App extends React.Component {
  constructor() {
    super();

    this.state = {
      arrayclickedImages: []
    }
  }

  /**
   * Обработчик, присваивающий значение url кликнутой картинки.
   * 
   * @param {string} url Параметр, содержащий url картинки.
   */
  handleClick = (url) => {
      this.setState({
        arrayclickedImages: [...this.state.arrayclickedImages, url]
      })
    }

  render() {
    const { arrayclickedImages } = this.state;

    return (
      <div className="main">
        <SavedImages
          data={arrayclickedImages}
        />
        <div className="app" id="1">
          <h3>Запросы в splashbase</h3>
          <TestInput
            onHandleClick={this.handleClick}
          />
        </div>
      </div>
    );
  }
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);